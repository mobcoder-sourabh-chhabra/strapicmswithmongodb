require('dotenv').config()

var strapi = {
"provider": "aws-s3",
"providerOptions": {
    "accessKeyId": process.env.accessKeyId,
    "secretAccessKey": process.env.secretAccessKey,
    "region": process.env.region,
    "params": {
      "Bucket": process.env.Bucket
    }
  }
}

module.exports = strapi;



